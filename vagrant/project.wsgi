import os
import sys

os.environ['DJANGO_SETTINGS_MODULE'] = '__PROJECT__.settings'
os.environ['PYTHON_EGG_CACHE'] = '/var/www/.python-eggs'
os.environ['APP_NAME'] = '__PROJECT__'
sys.path.append('/vagrant/__PROJECT__')

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
