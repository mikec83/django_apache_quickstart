project=mike
dbuser=mike
dbpassword=mike
dbname=mike

#echo on
sudo yum clean all -y
sudo yum update -y
sudo yum install epel-release -y
sudo yum install python34-devel python34-setuptools python34-pip -y
#sudo yum install httpd httpd-devel gcc -y

echo " "
echo "PostgreSQL"
echo " "
sudo yum install -y postgresql-server postgresql-devel postgresql-contrib
sudo postgresql-setup initdb
sudo cp /vagrant/vagrant/pg_hba.conf /var/lib/pgsql/data/
sed -i 's|__DBNAME__|'"${dbname}"'|g' /var/lib/pgsql/data/pg_hba.conf
sed -i 's|__DBUSER__|'"${dbuser}"'|g' /var/lib/pgsql/data/pg_hba.conf
sudo chown postgres:postgres /var/lib/pgsql/data/pg_hba.conf
sudo systemctl enable postgresql
sudo systemctl start postgresql

sudo -u postgres createuser --superuser ${dbuser}
sudo -u postgres createdb --owner=${dbuser} ${dbname}
sudo -u postgres psql -c "ALTER ROLE ${dbuser} WITH PASSWORD '${dbpassword}';"
sudo -u postgres psql -c "ALTER ROLE postgres WITH PASSWORD 'postgres';"

echo " "
echo "Python Virtual Env"
echo " "
sudo pip3 install --upgrade pip
sudo pip3 install virtualenv
sudo pip3 install mod_wsgi
cd /vagrant
virtualenv env --no-site-packages
source /vagrant/env/bin/activate
pip install django==2.0
pip install psycopg2

echo " "
echo "DJANGO SETUP"
echo " "
source /vagrant/env/bin/activate
cd /vagrant
django-admin startproject ${project}

cd /vagrant/${project}
cp /vagrant/vagrant/settings.py /vagrant/${project}/${project}/local_settings.py
sed -i 's|__PROJECT__|'"${project}"'|g' /vagrant/${project}/${project}/local_settings.py
sed -i 's|__DBUSER__|'"${dbuser}"'|g' /vagrant/${project}/${project}/local_settings.py
sed -i 's|__DBNAME__|'"${dbname}"'|g' /vagrant/${project}/${project}/local_settings.py
sed -i 's|__DBPASSWORD__|'"${dbpassword}"'|g' /vagrant/${project}/${project}/local_settings.py
cat >> /vagrant/${project}/${project}/settings.py << EOF_BASHRC
try:
    from .local_settings import *
except:
    import logging
    import traceback
    logging.error(traceback.format_exc())
EOF_BASHRC
./manage.py migrate
./manage.py collectstatic --noinput

echo "CREATING USER ADMIN WITH PASSWORD password123"
./manage.py loaddata /vagrant/vagrant/admin.json

echo " "
echo "Apache"
echo " "
sudo yum install httpd httpd-devel gcc -y
rm -rf /etc/httpd/conf.d/welcome.conf
usermod -G vagrant apache
ln -s /vagrant/${project} /var/www/${project}
ln -s /vagrant /opt/${project}
#sudo cp /vagrant/vagrant/django.conf /etc/httpd/conf.d/
cp /vagrant/vagrant/project-vagrant-wsgi.conf /etc/httpd/conf.d/${project}-vagrant-wsgi.conf
cp /vagrant/vagrant/project.wsgi /etc/httpd/conf.d/${project}-vagrant.wsgi
cp /vagrant/vagrant/project-vagrant.conf /etc/httpd/conf.d/${project}-vagrant.conf
sed -i 's|__PROJECT__|'"${project}"'|g' /etc/httpd/conf.d/*
systemctl enable httpd
sudo pip3 install mod_wsgi
sudo mkdir -p /var/run/wsgi/
sudo chown apache:apache /var/run/wsgi
systemctl start httpd

sudo firewall-cmd --permanent --add-port=80/tcp
sudo firewall-cmd --permanent --add-port=443/tcp
sudo firewall-cmd --reload

cat >> /home/vagrant/.bashrc <<EOF_BASHRC
alias python=/vagrant/${project}/bin/python
alias runserver='cd /vagrant python manage.py runserver 0.0.0.0:8000'
#clean up PYC
alias cleanup='find . -name "*.pyc" -exec rm {} \; -print'
source /vagrant/env/bin/activate
EOF_BASHRC
