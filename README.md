# Simple Vagrantfile to stand up Django running in mod_wsgi in Apache with PostgreSQL

    .______       _______     ___       _______  .___  ___.  _______ 
    |   _  \     |   ____|   /   \     |       \ |   \/   | |   ____|
    |  |_)  |    |  |__     /  ^  \    |  .--.  ||  \  /  | |  |__   
    |      /     |   __|   /  /_\  \   |  |  |  ||  |\/|  | |   __|  
    |  |\  \----.|  |____ /  _____  \  |  '--'  ||  |  |  | |  |____ 
    | _| `._____||_______/__/     \__\ |_______/ |__|  |__| |_______|
                                                                 
                                                                 
* Django 2.0
* Centos 7
* Python 3.4
* PostgreSQL

## Instructions

* edit the `vagrant/provision.sh` file to set the dbuser, dbname, dbpassword, and project values to your liking
* `vagrant up`
* Access your new django instace at http://127.0.0.1:8010

