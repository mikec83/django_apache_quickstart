Vagrant.configure("2") do |config|
  config.vm.box = "bento/centos-7.4"
  config.vm.synced_folder ".", "/vagrant"
  script = "vagrant/provision.sh"
  host = RbConfig::CONFIG['host_os']

  # Give VM 1/4 system memory 
  if host =~ /darwin/
    # sysctl returns Bytes and we need to convert to MB
    mem = `sysctl -n hw.memsize`.to_i / 1024
  elsif host =~ /linux/
    # meminfo shows KB and we need to convert to MB
    mem = `grep 'MemTotal' /proc/meminfo | sed -e 's/MemTotal://' -e 's/ kB//'`.to_i 
  elsif host =~ /mswin|mingw|cygwin/
    # Windows code via https://github.com/rdsubhas/vagrant-faster
    mem = `wmic computersystem Get TotalPhysicalMemory`.split[1].to_i / 1024
  end

  mem = mem / 1024 / 4
  
  config.vm.network  :forwarded_port, guest: 8000, host: 8009
  config.vm.network  :forwarded_port, guest: 80, host: 8010
  if host =~ /darwin/
    # Required for NFS to work, pick any local IP
    config.vm.network :private_network, ip: '192.168.50.50'
    # Use NFS for shared folders for better performance
    # Not necessary but helps on Mac OS X - https://stefanwrobel.com/how-to-make-vagrant-performance-not-suck
    config.vm.synced_folder '.', '/vagrant', nfs: true        
  end
  config.vm.provider 'virtualbox' do |vb|
        vb.customize ["modifyvm", :id, "--cableconnected1", "on"]
        vb.memory = mem
        vb.cpus = "2"        
    end
  config.vm.provision "shell", upload_path: "/home/vagrant/vagrant-shell", path: script
end
